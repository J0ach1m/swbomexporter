﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JS.Library.File
{
    class File
    {

        /// <summary>
        /// Returns the full path of a found file or null
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="rootPath"></param>
        /// <param name="filetype"></param>
        /// <param name="subfolderlevel"></param>
        /// <returns></returns>
        public static string FindFileByName(string filename, string rootPath, string filetype = "*.xml", int subfolderlevel = 4, string[] skippedFoldersByName = null)
        {

            //Console.WriteLine("findfilebyname, filename=" + filename + " rootpath=" + rootPath + " subfolderlevel=" + subfolderlevel.ToString());
            if (rootPath == null || filename == null) { return null; }
            if (rootPath.Length == 0 || filename.Length == 0 || filetype.Length == 0 || subfolderlevel < 0) { return null; }

            if (subfolderlevel == 0)
            {
                return null;
            }

            // Skipping folders not desired
            if (skippedFoldersByName != null && skippedFoldersByName.Length > 0)
            {
                foreach (string s in skippedFoldersByName)
                {
                    if (rootPath.Contains(s))
                    {
                        //if (infoMsg != null)
                        //{
                        //    //infoMsg?.Invoke(Helper.GetCurrentMethod() + " skipping folder " + rootPath);
                        //}
                        return null;
                    }
                }

            }
            //infoMsg(Helper.GetCurrentMethod() + " path=" + rootPath);



            System.IO.FileInfo[] files = null;
            System.IO.DirectoryInfo[] subdirs = null;
            System.IO.DirectoryInfo root = new DirectoryInfo(rootPath);
            try
            {
                files = root.GetFiles(filetype);
            }
            catch (UnauthorizedAccessException e)
            {
                //log.Add(e.Message);
                //if (errMsg != null)
                //{
                //    errMsg?.Invoke(Helper.GetCurrentMethod() + e.Message);
                //}
            }
            catch (System.IO.DirectoryNotFoundException e)
            {
                //Console.WriteLine(e.Message);
                //if (errMsg != null)
                //{
                //    errMsg?.Invoke(Helper.GetCurrentMethod() + e.Message);
                //}
            }
            catch (Exception e)
            {
                //if (errMsg != null)
                //{
                //    errMsg?.Invoke(Helper.GetCurrentMethod() + e.Message);
                //}
            }

            if (files != null)
            {
                foreach (System.IO.FileInfo fi in files)
                {
                    if (fi.Name.Contains(filename))
                    {
                        return fi.FullName;
                    }
                }
            }
            subdirs = root.GetDirectories();
            foreach (System.IO.DirectoryInfo dirInfo in subdirs)
            {
                string res = FindFileByName(filename, dirInfo.FullName, filetype, subfolderlevel - 1, skippedFoldersByName);
                if (res == null)
                {
                    continue;
                }
                else
                {
                    return res;
                }

            }
            return null;
        }
    }
}
