﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace JS.Library.File
{
    class Xml
    {
        public static XElement GetElementByAttributeNameAndValue(string attributename, string attributevalue, XDocument xdoc, string rootname="")
        {
            List<XElement> els = xdoc.Descendants(rootname).Descendants().ToList();
            string attribval = attributevalue.ToLowerInvariant();
            string attribname = attributename.ToLowerInvariant();
            //XElement r1 = els.First(e => e.Attribute(attribname).Value == attribval);
            XElement res = els.FirstOrDefault(el => el.Attribute(attribname) != null &&
                el.Attribute(attribname).Value.ToString().Equals(attribval));
            if (res != null)
            {
                return res;
            }
            return null;
        }

        public static string GetAttributeValue(XElement xe, string attributename)
        {
            if (xe.Attribute(attributename) != null)
            {
                return xe.Attribute(attributename).Value;
            }
            return null;
        }
    }
}
