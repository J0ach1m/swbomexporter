﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace JS.Library.Helper
{
    class Objects
    {
        public static bool SetPropertyValue(object obj, string propertyName, object propertyVal)
        {
            if(obj==null || string.IsNullOrWhiteSpace(propertyName))
            {
                return false;
            }

            Type objectType = obj.GetType();

            PropertyInfo prop = objectType.GetProperty(propertyName,BindingFlags.Instance|BindingFlags.Public|BindingFlags.NonPublic|BindingFlags.IgnoreCase);

            if(prop!=null && prop.CanWrite)
            {
                Type propertyType = prop.PropertyType;
                Type dataType = propertyType;

                // Check if type is nullable
                if(propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    if(propertyVal == null || string.IsNullOrWhiteSpace(propertyVal.ToString()))
                    {
                        prop.SetValue(obj, null);
                        return true;
                    }
                }
                else
                {
                    try
                    {
                        //dataType = propertyType.GetGenericArguments()[0];
                    }
                    catch(Exception e)
                    {

                    }
                }
                propertyVal = Convert.ChangeType(propertyVal, propertyType);
                prop.SetValue(obj, propertyVal);
                return true;
            }
            return false;

        }
    }
}
