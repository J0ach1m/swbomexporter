﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace JS.Library.Helper
{
    public class MethodSig
    {
        public string mName { get; set; }
        public Type returnType { get; set; }
        public Type[] parameterTypes { get; set; }

        public MethodSig(string name, Type returnType, Type[] parameterTypes)
        {
            this.mName = name;
            this.returnType = returnType;
            this.parameterTypes = parameterTypes;
        }
    }

    public class Reflections
    {
        public static string GetWorkingDirectory()
        {
            string assemblyFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            return assemblyFolder;
        }

        public static dynamic LoadDll(string path)
        {
            if (!System.IO.File.Exists(path))
            {
                return null;
            }
            
            try
            {
               var dll = Assembly.LoadFile(path);
               return dll;
            }
            catch (Exception e)
            {
                return null;
            }
        } 

        public static IEnumerable<MethodInfo> GetMethodsBySig(Type type, string name, Type returnType, params Type[] parameterTypes)
        {
            List<MethodInfo> ms = new List<MethodInfo>();
            foreach(MethodInfo m in type.GetMethods())
            {
                if (m.Name != name)
                {
                    continue;
                }
                if(m.ReturnType != returnType)
                {
                    continue;
                }
                var parameters = m.GetParameters();
                if ((parameterTypes == null || parameters.Length != parameterTypes.Length))
                {
                    continue;
                }
                else
                {
                    bool ok = true;
                    for (int i = 0; i < parameterTypes.Length; ++i)
                    {
                        if (parameters[i].ParameterType != parameterTypes[i])
                        {
                            ok = false;
                        }
                    }
                    if (ok)
                    {
                        ms.Add(m);
                    }
                }
            }
            return ms;
            //return type.GetMethods().Where((m) =>
            //{
            //    if (m.ReturnType != returnType)
            //    {
            //        return false;
            //    }
            //    var parameters = m.GetParameters();
            //    if ((parameterTypes == null || parameterTypes.Length == 0))
            //    {
            //        return parameters.Length == 0;
            //    }
            //    if (parameters.Length != parameterTypes.Length)
            //    {
            //        return false;
            //    }
            //    for (int i = 0; i < parameterTypes.Length; ++i)
            //    {
            //        if (parameters[i].ParameterType != parameterTypes[i])
            //        {
            //            return false;
            //        }
            //    }
            //    return true;
            //});
        }


        public static List<string> ContainingClasses(Assembly dll, List<MethodSig> signatures, out string errormsg)
        {
            StringBuilder sb = new StringBuilder();
            
            try
            {
                List<string> names = new List<string>();
                foreach (Type tpe in dll.GetTypes())
                {
                    bool has = true;
                    foreach(MethodSig sig in signatures)
                    {
                        var methods = GetMethodsBySig(tpe, sig.mName, sig.returnType, sig.parameterTypes);
                        if (!(methods==null || methods.Count()<=0))
                        {
                            has = true;
                        }
                        else
                        {
                            has = false;
                        }
                    }
                    if (has)
                    {
                        names.Add(tpe.Name);
                    }
                }
                errormsg = sb.ToString();
                return names;
            }
            catch(ReflectionTypeLoadException ex)
            {
                sb.Append("\n IC-ReflectionTypeLoadException:\n");
                foreach (var item in ex.LoaderExceptions)
                {
                    sb.Append("\t"+item.Message+"\n");
                }
                errormsg = sb.ToString();
                return null;
            }
            catch (Exception e)
            {
                
                sb.Append("\n" + "IC-Exception: " + e.Message + "\n");
                errormsg = sb.ToString();
                return null;
            }
        }

        public static List<string> ImplementingClasses(Assembly dll, Type implementedInterface, out string errormsg )
        {
            StringBuilder sb = new StringBuilder();
            if(dll==null || implementedInterface == null)
            {
                if (dll == null)
                {
                    errormsg = "IC: dll == null";
                }
                else
                {
                    errormsg = "IC: interface==null";
                }
                return null;
            }

            try
            {
                List<string> names = new List<string>();
                foreach(Type tpe in dll.GetTypes())
                {
                    if (implementedInterface.IsAssignableFrom(tpe) && !tpe.IsInterface)
                    {
                        names.Add(tpe.Name);
                    }
                }
               
                errormsg = sb.ToString();
                return names;
            }
            catch(ReflectionTypeLoadException ex)
            {
                sb.Append("\n IC-ReflectionTypeLoadException:\n");
                foreach (var item in ex.LoaderExceptions)
                {
                    sb.Append("\t"+item.Message+"\n");
                }
                errormsg = sb.ToString();
                return null;
            }
            catch (Exception e)
            {
                
                sb.Append("\n" + "IC-Exception: " + e.Message + "\n");
                errormsg = sb.ToString();
                return null;
            }
            
            
        }


        public static object CreateInstance(Assembly dll, string classname)
        {
            if (dll == null)
            {
                return null;
            }

            var tpe = (from type in dll.GetTypes() where type.Name == classname select type).FirstOrDefault();
            object instance = Activator.CreateInstance(tpe);
            return instance;
        }

        public static List<Tuple<Type,string>> GetAssembliesAndTypes(string wd, List<MethodSig> sigs, out string errormessage)
        {
            StringBuilder sb = new StringBuilder();
            if (!Directory.Exists(wd) || sigs == null || sigs.Count<=0)
            {
                errormessage = "directory does not exist or interface==null";
                return null;
            }
            List<Tuple<Type, string>> types = new List<Tuple<Type, string>>();
            if (types == null || types.Count <= 0)
            {
                sb.Append("types==null || types.count<=0\n");
            }

            foreach (var file in Directory.GetFiles(wd, "*.dll"))
            {
                //Console.Write("trying to load " + file);
                if (JS.Library.Helper.Reflections.LoadDll(file) != null)
                {
                    System.Reflection.Assembly dll = JS.Library.Helper.Reflections.LoadDll(file);

                    string errIC = "";
                    //List<string> classes = JS.Library.Helper.Reflections.ImplementingClasses(dll, typeof(JS.Library.SolidWorks.SWAssembly.ISolidWorksBomExport), out errIC);

                   
                    List<string> classes = JS.Library.Helper.Reflections.ContainingClasses(dll, sigs, out errIC);


                    sb.Append(errIC);
                    if (classes != null && classes.Count > 0)
                    {
                        //Console.Write(" - " + classes.Count().ToString() + " classes that implement desired interfaces:");
                        foreach (string c in classes)
                        {
                            //string cc = string.Concat(c, ",", dll.GetName().Name);
                            string cc = string.Concat(dll.GetName().Name, ".", c);

                            Type tpe = dll.GetType(cc, false, true);


                            types.Add(Tuple.Create(tpe, c));

                            //Console.Write(c + " ");
                        }
                        // Console.WriteLine();
                    }
                    else
                    {
                        if (classes == null)
                        {
                            sb.Append("file " + file + ": classes == null\n");
                        }
                        else if (classes.Count <= 0)
                        {
                            sb.Append("file " + file + ": classes.count <= 0\n");
                        }

                    }
                }
                else
                {
                    sb.Append("unable to load dll " + file + "\n");
                }
            }
            errormessage = sb.ToString();
            return types;
        }


        [MethodImpl(MethodImplOptions.NoInlining)]
        public static string GetCurrentMethod()
        {
            StackTrace st = new StackTrace();
            StackFrame sf = st.GetFrame(1);
            string s = "Method " + sf.GetMethod().Name;
            return s;
        }
        //public static List<Tuple<Type, string>> GetAssembliesAndTypes(string wd, Type implementedIterfaces, out string errormessage)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    if (!Directory.Exists(wd) || implementedIterfaces == null)
        //    {
        //        errormessage = "directory does not exist or interface==null";
        //        return null;
        //    }
        //    List<Tuple<Type, string>> types = new List<Tuple<Type, string>>();
        //    if(types==null || types.Count <= 0)
        //    {
        //        sb.Append("types==null || types.count<=0\n");
        //    }

        //    foreach (var file in Directory.GetFiles(wd, "*.dll"))
        //    {
        //        //Console.Write("trying to load " + file);
        //        if (JS.Library.Helper.Reflections.LoadDll(file) != null)
        //        {
        //            System.Reflection.Assembly dll = JS.Library.Helper.Reflections.LoadDll(file);

        //            string errIC="";
        //            List<string> classes = JS.Library.Helper.Reflections.ImplementingClasses(dll, typeof(JS.Library.SolidWorks.SWAssembly.ISolidWorksBomExport), out errIC);
        //            sb.Append(errIC);
        //            if (classes != null && classes.Count > 0)
        //            {
        //                //Console.Write(" - " + classes.Count().ToString() + " classes that implement desired interfaces:");
        //                foreach (string c in classes)
        //                {
        //                    //string cc = string.Concat(c, ",", dll.GetName().Name);
        //                    string cc = string.Concat(dll.GetName().Name,".",c);

        //                    Type tpe = dll.GetType(cc, false, true);


        //                    types.Add(Tuple.Create(tpe, c));

        //                    //Console.Write(c + " ");
        //                }
        //                // Console.WriteLine();
        //            }
        //            else
        //            {
        //                if (classes == null)
        //                {
        //                    sb.Append("file " + file + ": classes == null\n");
        //                }
        //                else if (classes.Count <= 0)
        //                {
        //                    sb.Append("file " + file + ": classes.count <= 0\n");
        //                }

        //            }
        //        }
        //        else
        //        {
        //            sb.Append("unable to load dll " + file + "\n");
        //        }
        //    }
        //    errormessage = sb.ToString();
        //    return types;
        //}

        public static List<object> CreateInstances(List<Tuple<Type, string>> types)
        {
            List<object> obj2 = new List<object>();
            if (types != null && types.Count > 0)
            {
                foreach (Tuple<Type, string> tpe in types)
                {
                    //Console.WriteLine(" Type " + tpe.Item1.Name + " found in assembly " + tpe.Item2);
                    Type tp = tpe.Item1;
                    var o = Activator.CreateInstance(tp);
                    obj2.Add(o); ;
                }
                return obj2;
            }
            return null;
        }
       

    }
}
