﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace JS.Library.Helper
{
    public class Strings
    {
        public static List<string> GetRegexMatches(string input, string pattern)
        {
            List<string> rmatches = new List<string>();
            Regex r = new Regex(pattern, RegexOptions.IgnoreCase);
            rmatches = r.Matches(input).Cast<Match>().Select(m => m.Value).ToList();
            return rmatches;
        }

        public static string GetFirstRegexMatch(string input, string pattern)
        {
            if(GetRegexMatches(input, pattern).Count > 0)
            {
                return GetRegexMatches(input, pattern)[0];
            }
            return null;
        }

        public static string RemoveRegexMatches(string input, string pattern)
        {
            return Regex.Replace(input, pattern, "");
        }

    }
}
