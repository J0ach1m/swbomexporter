﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JS.Library.SolidWorks.SWAssembly
{
    public class AsmComponent
    {
        public string componenttypename { get; set; }
        public string rawdata { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public int majorversion { get; set; }
        public int minorversion { get; set; }
        public int quantity { get; set; }
        public string type { get; set; }
        public string componenttype { get; set; }
        public string parent_id { get; set; }
        public int hierarchy_level { get; set; }
        public string subtype { get; set; }

        
        public AsmComponent()
        {
            this.componenttypename = "";
            this.quantity = 1;
            this.rawdata = "";
            this.id = "";
            this.name = "";
            this.majorversion = 0;
            this.minorversion = 0;
            this.type = "";
            this.componenttype = "";
            this.subtype = "";
            this.parent_id = "";
            this.hierarchy_level = -1;
        }

        public override string ToString()
        {
            string s = string.Format("{0};{1}.{2};{3};{4};{5};{6}",this.id,this.majorversion,this.minorversion,this.name,this.subtype, this.quantity,this.rawdata);
            return s;
        }

       
    }
}
