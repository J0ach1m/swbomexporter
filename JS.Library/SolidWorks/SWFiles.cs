﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JS.Library.SolidWorks
{
    public class SWFiles
    {
        public static List<string> ProductionFilenames(List<string> substrings, List<string> extensions)
        {
            List<string> generated_filenames = new List<string>();
            StringBuilder sb = new StringBuilder();
            foreach(string s in substrings)
            {
                sb.Append(s);
            }
            sb.Append(".");

            foreach(string ext in extensions)
            {
                generated_filenames.Add(sb.ToString() + ext);
            }
            return generated_filenames;
        }


    }
}
