﻿using SolidWorks.Interop.sldworks;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace JS.Library.SolidWorks
{
    public static class SWInstances
    {
        static public IEnumerable<SldWorks> Solidworks()
        {
           List<SldWorks> instances = new List<SldWorks>();
            try
            {
                //return (SldWorks[])Helper.ROTHelper.GetActiveObjectList(null)
                //    .Where(keyvalue => keyvalue.Key.ToLower().Contains("solidworks"))
                //    .Select(keyvalue=>keyvalue.Value).ToArray();

                //return (SldWorks)Helper.ROTHelper.GetActiveObjectList(null)
                //    .Where(keyvalue => keyvalue.Key.ToLower().Contains("solidworks"))
                //    .Select(keyvalue => keyvalue.Value).First();

                foreach(KeyValuePair<string, object> obj in Helper.ROTHelper.GetActiveObjectList(null))
                {
                    if(obj.Key.ToLower().Contains("solidworks"))
                    {
                        instances.Add((SldWorks)obj.Value);
                    }
                }
                return instances;

            }
            catch
            {
                return null;
            }
        }
    }
}
