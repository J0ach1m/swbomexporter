﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BomExportMP
{
    public class BomExportMP
    {
        public string GetAssemblyName()
        {
            return this.GetType().Name;
        }
        
        public bool PushData(List<string> AssemblyContent, string contentFormat)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(GetAssemblyName() + "PushData:\n");
            foreach(string s in AssemblyContent)
            {
                sb.Append(s + "\n");
            }
            MessageBox.Show(sb.ToString(),"PushData",MessageBoxButtons.OK,MessageBoxIcon.Information);
            //throw new NotImplementedException();
            return true;
        }
    }
}
