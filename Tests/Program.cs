﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JS.Library;
using System.Xml.Linq;
using SolidWorks.Interop.sldworks;
using System.Diagnostics;
using SolidWorks.Interop.swconst;
using SWBlankAddin;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace Tests
{
    class Program
    {
         

        static void Main(string[] args)
        {
            SldWorks swApp;
            ModelDoc2 swModel;
            ModelDocExtension swExt;
            SelectionMgr swSelMgr;
            Component2 swComp;
            ConfigurationManager swConfMgr;
            Configuration swConf;
            Component2 swRootComp;
            AssemblyDoc swAssem;
            FeatureManager featMgr;


            //test: find dll's that contain classes that implement ISolidWorksBomExport-interface
            string wd = JS.Library.Helper.Reflections.GetWorkingDirectory();
            string errormsg = "";
            //List<Tuple<Type, string>> types = JS.Library.Helper.Reflections.GetAssembliesAndTypes(wd, typeof(JS.Library.SolidWorks.SWAssembly.ISolidWorksBomExport), out errormsg);


            List<JS.Library.Helper.MethodSig> signatures = new List<JS.Library.Helper.MethodSig>();
            //find: List<string> AssemblyContent, string contentFormat
            signatures.Add(new JS.Library.Helper.MethodSig("PushData",typeof(bool), new Type[] { typeof(List<string>),typeof(string) }));
            signatures.Add(new JS.Library.Helper.MethodSig("GetAssemblyName",typeof(string), new Type[] {}));


            List<Tuple<Type, string>> types = JS.Library.Helper.Reflections.GetAssembliesAndTypes(wd, signatures, out errormsg);

            if(types!=null && types.Count > 0)
            {
                List<object> obj2 = JS.Library.Helper.Reflections.CreateInstances(types);

                foreach (object o in obj2)
                {
                    MethodInfo mi = o.GetType().GetMethod("GetAssemblyName");
                    Console.WriteLine("o.getassemblyname: " + (mi.Invoke(o,null)));
                }
            }



            


            try
            {


                //swApp = (SldWorks)System.Runtime.InteropServices.Marshal.GetActiveObject("SldWorks.Application");

                // Test Get Sldworks instances
                List<SldWorks> instances = JS.Library.SolidWorks.SWInstances.Solidworks().ToList();

                if (instances != null && instances.Count > 0)
                {
                    
                    if (instances.Count > 1)
                    {
                        List<string> names = new List<string>();

                        foreach (SldWorks sw in instances)
                        {
                            string name="";
                            try
                            {
                                name = ((ModelDoc2)(sw.ActiveDoc())).GetTitle();
                                names.Add(((ModelDoc2)sw.ActiveDoc()).GetTitle());
                            }
                            catch(Exception e)
                            {
                                name = e.Message;
                            }
                            names.Add(name);
                            //SWBoMExport.SWInstanceSelector swis = new SWBoMExport.SWInstanceSelector(names);
                            //swis.Show();
                        }


                        //MessageBox.Show("Multiple SolidWorks instances found. Close unneeded or choose desired instance below.");
                    }
                    //swApp = (SldWorks)instances[0];
                        
                    else
                    {
                        swApp = (SldWorks)System.Runtime.InteropServices.Marshal.GetActiveObject("SldWorks.Application");
                    }
                    

                    swApp = (SldWorks)instances[0];
                    swModel = (ModelDoc2)swApp.ActiveDoc;
                    if (swModel == null)
                    {
                        return;
                    }
                    swExt = (ModelDocExtension)swModel.Extension;
                    swSelMgr = (SelectionMgr)swModel.SelectionManager;
                    swConfMgr = (ConfigurationManager)swModel.ConfigurationManager;
                    swConf = (Configuration)swConfMgr.ActiveConfiguration;
                    swRootComp = (Component2)swConf.GetRootComponent();
                    
                    swAssem = (AssemblyDoc)swModel;
                }
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                return;
            }

           

            if (swModel.GetType() != (int)swDocumentTypes_e.swDocASSEMBLY)
            {
                return;
            }

            object[] vComponents = (object[])swAssem.GetComponents(true);
            string path = JS.Library.Helper.Reflections.GetWorkingDirectory();
            path = System.IO.Path.Combine(path, "config.xml");

            //System.IO.File.WriteAllText(@"C:\test\configpath.txt", path);
            XDocument xdoc;
            if (System.IO.File.Exists(path))
            {
                xdoc = XDocument.Load(path);
            }
            else
            {
                xdoc = XDocument.Load(@"C:\test\config.xml");
            }

          

          
          

            List<JS.Library.SolidWorks.SWAssembly.AsmComponent> results = JS.Library.SolidWorks.SWAssembly.SWAssemblyHelper.GetDataFromAssemblyEntries(vComponents,
                 xdoc);

            // 2018-04-09........
            StringBuilder sb0 = new StringBuilder();
            foreach (JS.Library.SolidWorks.SWAssembly.AsmComponent c in results)
            {
                if (c != null)
                {
                    sb0.Append(c.ToString() + "\n");
                }
                
            }
            string msgtext0 = sb0.ToString();







                      
            
            Console.WriteLine(msgtext0);



            // testing populating list stuff
            Console.WriteLine();
            foreach (JS.Library.SolidWorks.SWAssembly.AsmComponent ac in results)
            {
                string v = string.Format("{0}.{1}", ac.majorversion.ToString(), ac.minorversion.ToString());

                string[] row = { ac.id, v, ac.name.ToString(), ac.subtype.ToString(), ac.quantity.ToString(), ac.rawdata };
                
                var listviewitem = new ListViewItem(row);
                
                foreach (string r in row)
                {
                    Console.Write(r+" ");
                }
                Console.WriteLine();
            }
           


            StringBuilder sb = new StringBuilder();
            sb.Append(results.Count().ToString() + " components of "
                + results.Select(x => x.componenttypename).Distinct().Count().ToString()
                + " different types found. (");
            foreach (string c in results.Select(x => x.componenttypename).Distinct())
            {
                sb.Append(results.Count(x => x.componenttypename.Equals(c)) + " " + c + ", ");
            }
            sb.Length = sb.Length - 2;
            sb.Append(")");
            string resulttext = sb.ToString();
            Console.WriteLine( resulttext);




            Console.ReadLine();
        }
    }
}
