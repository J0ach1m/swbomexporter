﻿using SolidWorks.Interop.sldworks;
using SolidWorks.Interop.swpublished;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SWBlankAddin
{
    public class SWAssembly:ISwAddin
    {
        #region Private Members
        /// <summary>
        /// Cookie to the current instance of Solidworks
        /// </summary>
        private int mSwCookie;

        /// <summary>
        /// Taskpane view for this addin
        /// </summary>
        private TaskpaneView mTaskpaneView;

        /// <summary>
        /// The UI control inside the SolidWorks taskpane view
        /// </summary>
        private TaskpaneHostUI mTaskpaneHost;

        /// <summary>
        /// instance of the solidworks application
        /// </summary>
        private SldWorks mSolidWorksApplication;
        #endregion

        #region Public Members

        /// <summary>
        /// Unique id for registration in COM
        /// </summary>
        public const string SWTASKPANE_PROGID = "JS.Solidworks.BoMAddin";
        #endregion


        #region Solidworks Add-in Callbacks

        /// <summary>
        /// Called when solidworks has loaded this add-in and wants tu do our connection logic
        /// </summary>
        /// <param name="ThisSW">current sw instance</param>
        /// <param name="Cookie">current sw cookie id</param>
        /// <returns></returns>
        public bool ConnectToSW(object ThisSW, int Cookie)
        {
            // store a reference to the current solidworks instance
            mSolidWorksApplication = (SldWorks)ThisSW;

            // store the cookie id
            mSwCookie = Cookie;

            // setup callback info
            var ok = mSolidWorksApplication.SetAddinCallbackInfo2(0, this, mSwCookie);

            string path = JS.Library.Helper.Reflections.GetWorkingDirectory();
            path = System.IO.Path.Combine(path, "config.xml");

            //System.IO.File.WriteAllText(@"C:\test\text.txt", path);

            LoadUI();

            return true;
        }

        

        /// <summary>
        /// called when solidworks is about to unload the addin, do the disconnection logic
        /// </summary>
        /// <returns></returns>
        public bool DisconnectFromSW()
        {
            UnloadUI();
            return true;
        }

        




        #endregion

        #region Create UI

        /// <summary>
        /// Create the taskpane and inject the host UI
        /// </summary>
        private void LoadUI()
        {
            // find logo location
            var imagePath = Path.Combine(JS.Library.Helper.Reflections.GetWorkingDirectory(), "logo.png");
            //var imagePath = Path.Combine(Path.GetDirectoryName(typeof(SWAssembly).Assembly.CodeBase).Replace(@"file:\", ""), "logo.png");
            //File.WriteAllText(@"C:\test\imagepath.txt",imagePath.ToString());
            // create taskpane
            mTaskpaneView= mSolidWorksApplication.CreateTaskpaneView2(imagePath, "BoM Addin");

            // load ui
            mTaskpaneHost = (TaskpaneHostUI)mTaskpaneView.AddControl(SWAssembly.SWTASKPANE_PROGID, string.Empty);
        }

        /// <summary>
        /// Cleanup the taspane on disconnect
        /// </summary>
        private void UnloadUI()
        {
            mTaskpaneHost = null;

            // remove taskpane view
            mTaskpaneView.DeleteView();

            // Release COM reference and clean memory
            Marshal.ReleaseComObject(mTaskpaneView);

            mTaskpaneView = null;
        }
        #endregion

        #region COM Registration

        /// <summary>
        /// The COM registration call to add the registry entries to the solidworks add-in registry
        /// </summary>
        /// <param name="t"></param>
        [ComRegisterFunction()]
        private static void ComRegister (Type t)
        {
            var keyPath = string.Format(@"SOFTWARE\Solidworks\AddIns\{0:b}", t.GUID);

            // create registry entry
            using (var rk = Microsoft.Win32.Registry.LocalMachine.CreateSubKey(keyPath))
            {
                // load add-in when solidworks opens
                rk.SetValue(null, 1);

                rk.SetValue("Title", "BoM Eport");
                rk.SetValue("Description", "Creates BoM Data from Assemblies");
            }
        }

        /// <summary>
        /// the COM unregister call to remove our custom entries we added in the COM register function
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        [ComUnregisterFunction()]
        private static void ComUnregister(Type t)
        {
            var keyPath = string.Format(@"SOFTWARE\Solidworks\AddIns\{0:b}", t.GUID);

            Microsoft.Win32.Registry.LocalMachine.DeleteSubKeyTree(keyPath);
        }
        #endregion

    }
   
}
