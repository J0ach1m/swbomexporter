﻿namespace SWBlankAddin
{
    partial class TaskpaneHostUI
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.RbtnUnsuppressed = new System.Windows.Forms.RadioButton();
            this.RBtnSuppressed = new System.Windows.Forms.RadioButton();
            this.RBtnCurrentViewstate = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.CheckedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.BtnPushData = new System.Windows.Forms.Button();
            this.Lbl2 = new System.Windows.Forms.Label();
            this.RBtnRecursive2 = new System.Windows.Forms.RadioButton();
            this.RBtnRecursivePartsOnly = new System.Windows.Forms.RadioButton();
            this.RBtnTopLevel = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.CbxExportMethod = new System.Windows.Forms.ComboBox();
            this.backgroundWorker2 = new System.ComponentModel.BackgroundWorker();
            this.TTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(62, 230);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(127, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Create BoM";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // RbtnUnsuppressed
            // 
            this.RbtnUnsuppressed.AutoSize = true;
            this.RbtnUnsuppressed.Checked = true;
            this.RbtnUnsuppressed.Location = new System.Drawing.Point(6, 19);
            this.RbtnUnsuppressed.Name = "RbtnUnsuppressed";
            this.RbtnUnsuppressed.Size = new System.Drawing.Size(221, 17);
            this.RbtnUnsuppressed.TabIndex = 1;
            this.RbtnUnsuppressed.TabStop = true;
            this.RbtnUnsuppressed.Text = "Current Configuration (only unsuppressed)";
            this.RbtnUnsuppressed.UseVisualStyleBackColor = true;
            this.RbtnUnsuppressed.CheckedChanged += new System.EventHandler(this.RbtnUnsuppressed_CheckedChanged);
            // 
            // RBtnSuppressed
            // 
            this.RBtnSuppressed.AutoSize = true;
            this.RBtnSuppressed.Location = new System.Drawing.Point(6, 65);
            this.RBtnSuppressed.Name = "RBtnSuppressed";
            this.RBtnSuppressed.Size = new System.Drawing.Size(36, 17);
            this.RBtnSuppressed.TabIndex = 2;
            this.RBtnSuppressed.Text = "All";
            this.RBtnSuppressed.UseVisualStyleBackColor = true;
            this.RBtnSuppressed.CheckedChanged += new System.EventHandler(this.RBtnSuppressed_CheckedChanged);
            // 
            // RBtnCurrentViewstate
            // 
            this.RBtnCurrentViewstate.AutoSize = true;
            this.RBtnCurrentViewstate.Location = new System.Drawing.Point(6, 42);
            this.RBtnCurrentViewstate.Name = "RBtnCurrentViewstate";
            this.RBtnCurrentViewstate.Size = new System.Drawing.Size(133, 17);
            this.RBtnCurrentViewstate.TabIndex = 3;
            this.RBtnCurrentViewstate.Text = "Viewstate (Only visible)";
            this.RBtnCurrentViewstate.UseVisualStyleBackColor = true;
            this.RBtnCurrentViewstate.CheckedChanged += new System.EventHandler(this.RBtnCurrentViewstate_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.CheckedListBox1);
            this.groupBox1.Controls.Add(this.RbtnUnsuppressed);
            this.groupBox1.Controls.Add(this.RBtnCurrentViewstate);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.RBtnSuppressed);
            this.groupBox1.Location = new System.Drawing.Point(3, 18);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(266, 259);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Component Selection";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // CheckedListBox1
            // 
            this.CheckedListBox1.FormattingEnabled = true;
            this.CheckedListBox1.Location = new System.Drawing.Point(6, 98);
            this.CheckedListBox1.Name = "CheckedListBox1";
            this.CheckedListBox1.Size = new System.Drawing.Size(254, 124);
            this.CheckedListBox1.TabIndex = 4;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.BtnPushData);
            this.groupBox2.Controls.Add(this.Lbl2);
            this.groupBox2.Controls.Add(this.RBtnRecursive2);
            this.groupBox2.Controls.Add(this.RBtnRecursivePartsOnly);
            this.groupBox2.Controls.Add(this.RBtnTopLevel);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.CbxExportMethod);
            this.groupBox2.Location = new System.Drawing.Point(3, 283);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(267, 182);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Export BoM";
            // 
            // BtnPushData
            // 
            this.BtnPushData.Location = new System.Drawing.Point(62, 149);
            this.BtnPushData.Name = "BtnPushData";
            this.BtnPushData.Size = new System.Drawing.Size(127, 23);
            this.BtnPushData.TabIndex = 10;
            this.BtnPushData.Text = "Push Data";
            this.BtnPushData.UseVisualStyleBackColor = true;
            this.BtnPushData.Click += new System.EventHandler(this.BtnPushData_Click);
            // 
            // Lbl2
            // 
            this.Lbl2.AutoSize = true;
            this.Lbl2.Location = new System.Drawing.Point(9, 52);
            this.Lbl2.Name = "Lbl2";
            this.Lbl2.Size = new System.Drawing.Size(10, 13);
            this.Lbl2.TabIndex = 8;
            this.Lbl2.Text = "-";
            // 
            // RBtnRecursive2
            // 
            this.RBtnRecursive2.AutoSize = true;
            this.RBtnRecursive2.Enabled = false;
            this.RBtnRecursive2.Location = new System.Drawing.Point(9, 117);
            this.RBtnRecursive2.Name = "RBtnRecursive2";
            this.RBtnRecursive2.Size = new System.Drawing.Size(260, 17);
            this.RBtnRecursive2.TabIndex = 7;
            this.RBtnRecursive2.TabStop = true;
            this.RBtnRecursive2.Text = "Recursive with indent (parts and assembly names)";
            this.RBtnRecursive2.UseVisualStyleBackColor = true;
            this.RBtnRecursive2.CheckedChanged += new System.EventHandler(this.RBtnRecursive2_CheckedChanged);
            // 
            // RBtnRecursivePartsOnly
            // 
            this.RBtnRecursivePartsOnly.AutoSize = true;
            this.RBtnRecursivePartsOnly.Enabled = false;
            this.RBtnRecursivePartsOnly.Location = new System.Drawing.Point(9, 94);
            this.RBtnRecursivePartsOnly.Name = "RBtnRecursivePartsOnly";
            this.RBtnRecursivePartsOnly.Size = new System.Drawing.Size(124, 17);
            this.RBtnRecursivePartsOnly.TabIndex = 3;
            this.RBtnRecursivePartsOnly.TabStop = true;
            this.RBtnRecursivePartsOnly.Text = "Recursive, parts only";
            this.RBtnRecursivePartsOnly.UseVisualStyleBackColor = true;
            this.RBtnRecursivePartsOnly.CheckedChanged += new System.EventHandler(this.RBtnRecursivePartsOnly_CheckedChanged);
            // 
            // RBtnTopLevel
            // 
            this.RBtnTopLevel.AutoSize = true;
            this.RBtnTopLevel.Checked = true;
            this.RBtnTopLevel.Enabled = false;
            this.RBtnTopLevel.Location = new System.Drawing.Point(9, 71);
            this.RBtnTopLevel.Name = "RBtnTopLevel";
            this.RBtnTopLevel.Size = new System.Drawing.Size(150, 17);
            this.RBtnTopLevel.TabIndex = 2;
            this.RBtnTopLevel.TabStop = true;
            this.RBtnTopLevel.Text = "Only top level components";
            this.RBtnTopLevel.UseVisualStyleBackColor = true;
            this.RBtnTopLevel.CheckedChanged += new System.EventHandler(this.RBtnTopLevel_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Export Method";
            // 
            // CbxExportMethod
            // 
            this.CbxExportMethod.FormattingEnabled = true;
            this.CbxExportMethod.Location = new System.Drawing.Point(88, 27);
            this.CbxExportMethod.Name = "CbxExportMethod";
            this.CbxExportMethod.Size = new System.Drawing.Size(166, 21);
            this.CbxExportMethod.TabIndex = 0;
            // 
            // TTip1
            // 
            this.TTip1.Popup += new System.Windows.Forms.PopupEventHandler(this.TTip1_Popup);
            // 
            // TaskpaneHostUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "TaskpaneHostUI";
            this.Size = new System.Drawing.Size(273, 706);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RadioButton RbtnUnsuppressed;
        private System.Windows.Forms.RadioButton RBtnSuppressed;
        private System.Windows.Forms.RadioButton RBtnCurrentViewstate;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox CbxExportMethod;
        private System.ComponentModel.BackgroundWorker backgroundWorker2;
        private System.Windows.Forms.RadioButton RBtnTopLevel;
        private System.Windows.Forms.RadioButton RBtnRecursive2;
        private System.Windows.Forms.RadioButton RBtnRecursivePartsOnly;
        private System.Windows.Forms.ToolTip TTip1;
        private System.Windows.Forms.Label Lbl2;
        private System.Windows.Forms.Button BtnPushData;
        private System.Windows.Forms.CheckedListBox CheckedListBox1;
    }
}
