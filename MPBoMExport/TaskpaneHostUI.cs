﻿/*A SolidWorks Addin that enables to output Assembly information to Bill of Material lists.
    Copyright(C) 2018 Joachim Spielbichler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.If not, see<https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SolidWorks.Interop.sldworks;
using System.Diagnostics;
using SolidWorks.Interop.swconst;
using JS.Library;
using System.Xml.Linq;
using System.Reflection;

namespace SWBlankAddin
{
    public enum AsmIncludeOptions { CurrentConfig, CurrentViewstate, SuppressedIncluded };
    public enum AsmExportOptions { TopLevel, RecursiveParts, RecursiveIndent };
    
    [System.Runtime.InteropServices.ProgId(SWBlankAddin.SWAssembly.SWTASKPANE_PROGID)]
    public partial class TaskpaneHostUI : UserControl
    {
        public AsmIncludeOptions asmIncludeOptions;
        public AsmExportOptions asmExportOptions;
        List<SldWorks> swInstances;
        public SldWorks swApp;
        private JS.Library.SolidWorks.SWAssembly.SWAssemblyHelper.ComponentSelection selection = JS.Library.SolidWorks.SWAssembly.SWAssemblyHelper.ComponentSelection.UNSUPPRESED;

        public List<object> objFromLoadedAssemblies;

        public TaskpaneHostUI()
        {
            InitializeComponent();
            try
            {
                swInstances = JS.Library.SolidWorks.SWInstances.Solidworks().ToList();

                if (swInstances != null && swInstances.Count > 0)
                {
                    swApp = (SldWorks)swInstances[0];
                }
                else
                {
                    return;
                }
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message,"Initialization error",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }

            // Set tooltip texts
            this.TTip1.SetToolTip(this.CbxExportMethod, "Shows all available export options provided through included dll's that contain classes implementing ISolidWorksBomExport Interface");

            PopulateAssembliesMenu();

           
        }

        /// <summary>
        /// Instantiates an object of each class that correspond to the requirements for
        /// an BoM Export Assembly
        /// </summary>
        public void PopulateAssembliesMenu()
        {
            string wd = JS.Library.Helper.Reflections.GetWorkingDirectory();
            string errmsg = "";
                       
            List<JS.Library.Helper.MethodSig> signatures = new List<JS.Library.Helper.MethodSig>();
            //find: List<string> AssemblyContent, string contentFormat
            signatures.Add(new JS.Library.Helper.MethodSig("PushData", typeof(bool), new Type[] { typeof(List<string>), typeof(string) }));
            signatures.Add(new JS.Library.Helper.MethodSig("GetAssemblyName", typeof(string), new Type[] { }));
            List<Tuple<Type, string>> types = JS.Library.Helper.Reflections.GetAssembliesAndTypes(wd, signatures, out errmsg);



            if (types==null || types.Count <= 0)
            {
                MessageBox.Show("No additional exporting methods found in " + wd+"\n"+errmsg);
            }
            else
            {
                this.objFromLoadedAssemblies = JS.Library.Helper.Reflections.CreateInstances(types);
                if (objFromLoadedAssemblies != null && objFromLoadedAssemblies.Count > 0)
                {
                    foreach (object o in objFromLoadedAssemblies)
                    {
                        ComboboxItem item = new ComboboxItem();
                        
                        MethodInfo mi = o.GetType().GetMethod("GetAssemblyName");
                        string text = (string)mi.Invoke(o, null);
                        
                        item.Text = text;
                        this.CbxExportMethod.Items.Add(item);
                        //this.Lbl2.Text = text;
                    }
                    this.CbxExportMethod.SelectedIndex = 0;
                }
                else
                {
                    MessageBox.Show(types.Count.ToString()+" types in " + wd+" found, none instantiated");
                }
            }
        }

        /// <summary>
        /// feasability study of solidworks api assembly bom export
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                swInstances = JS.Library.SolidWorks.SWInstances.Solidworks().ToList();

                if (swInstances != null && swInstances.Count > 0)
                {
                    int swInstance = 0;
                    if (swInstances.Count > 1)
                    {
                        List<string> names = new List<string>();
                        int count=0;
                        foreach (SldWorks sw in swInstances)
                        {
                            string name = "";
                            try
                            {
                                name = ((ModelDoc2)(sw.ActiveDoc())).GetTitle();
                                names.Add(((ModelDoc2)sw.ActiveDoc()).GetTitle());
                            }
                            catch (Exception ex)
                            {
                                //name = ex.Message;
                                name = string.Format("instance {0}", count);
                            }
                            names.Add(name);
                            ++count;
                        }
                        //names.Add(((ModelDoc2)sw.ActiveDoc()).GetTitle());
                        SWBoMExport.SWInstanceSelector swis = new SWBoMExport.SWInstanceSelector(names);
                      
                        var res = swis.ShowDialog();
                        if(res == DialogResult.OK)
                        {
                            swInstance = swis.choosenInstance;
                        }
                        
                    }
                    swApp = (SldWorks)swInstances[swInstance];
                }
                else
                {
                    swApp = (SldWorks)System.Runtime.InteropServices.Marshal.GetActiveObject("SldWorks.Application");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Initialization error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            ModelDoc2 swModel;
            ModelDocExtension swExt;
            SelectionMgr swSelMgr;
            Component2 swComp;
            ConfigurationManager swConfMgr;
            Configuration swConf;
            Component2 swRootComp;
            AssemblyDoc swAssem;
            FeatureManager featMgr;

           
            swModel = (ModelDoc2)swApp.ActiveDoc;
            if (swModel == null)
            {
                MessageBox.Show("No SW model found");
                return;
            }
            swExt = (ModelDocExtension)swModel.Extension;
            swSelMgr = (SelectionMgr)swModel.SelectionManager;
            swConfMgr = (ConfigurationManager)swModel.ConfigurationManager;
            swConf = (Configuration)swConfMgr.ActiveConfiguration;
            swRootComp = (Component2)swConf.GetRootComponent();
            swAssem = (AssemblyDoc)swModel;
           

            if (swModel.GetType() != (int)swDocumentTypes_e.swDocASSEMBLY)
            {
                return;
            }


            // TODO: Determine which type of component and to wich level should be included
            // e.g. get all unsuppressed objects, visible and hidden
            object[] vComponents = (object[])swAssem.GetComponents(true);
            List<object> cleanedComponents;
           
            cleanedComponents = JS.Library.SolidWorks.SWAssembly.SWAssemblyHelper.SelectComponents(vComponents,selection);
            

            object[] cleaned = cleanedComponents.ToArray();
            //swComp = (Component2)vComponents[0];
            
            List<JS.Library.SolidWorks.SWAssembly.AsmComponent> entries = new List<JS.Library.SolidWorks.SWAssembly.AsmComponent>();
           
            string asmConfiguration = swConf.Name;
            
            try
            {
                string path = JS.Library.Helper.Reflections.GetWorkingDirectory();
                path = System.IO.Path.Combine(path, "config.xml");

                //System.IO.File.WriteAllText(@"C:\test\configpath.txt", path);
                XDocument xdoc;
                if (System.IO.File.Exists(path))
                {
                    xdoc = XDocument.Load(path);
                }
                else
                {
                    xdoc = XDocument.Load(@"C:\test\config.xml");
                }
                
                List<JS.Library.SolidWorks.SWAssembly.AsmComponent> results = JS.Library.SolidWorks.SWAssembly.SWAssemblyHelper.GetDataFromAssemblyEntries(cleaned, 
                    xdoc);
                
                StringBuilder sb0 = new StringBuilder();
                foreach (JS.Library.SolidWorks.SWAssembly.AsmComponent c in results)
                {
                    if (c != null)
                    {
                        sb0.Append(c.ToString() + "\n");
                    }
                }
                string msgtext0 = sb0.ToString();
                
                Clipboard.SetText(msgtext0);

                OutputWindow ow = new OutputWindow();
                ow.Show();
                ow.PopulateDataTable(results);

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


            

            #region old




            //bool onlyunsuppressed = true;

            //string asmPattern = @"A[0-9]{5}V[0-9]{1,2}p[0-9]{1,2}";
            //string prtPattern = @"[0-9]{6}V[0-9]{1,2}p[0-9]{1,2}";
            //string stdPattern = @"N[0-9]{6}";
            //string majVersionPattern = @"V[0-9]{1,2}";
            //string minVersionPattern = @"p[0-9]{1,2}";
            //string asmIdPattern = @"A[0-9]{5}";
            //string prtIdPattern = @"[0-9]{6}";

            //foreach (Component2 c in vComponents)
            //{
            //    string docPath = c.GetPathName();
            //    if(onlyunsuppressed && !c.IsSuppressed())
            //    {
            //        List<string> matches = new List<string>();
            //        string id = "";
            //        string n = "";
            //        int majorrelease = -1, minorrelease = -1;

            //        if (JS.Library.Helper.Strings.GetRegexMatches(c.Name, asmPattern).Count > 0)
            //        {
            //            matches = JS.Library.Helper.Strings.GetRegexMatches(c.Name, asmPattern);
                        
            //            // Get assembly 
            //            List<string> ids = new List<string>();
            //            ids = JS.Library.Helper.Strings.GetRegexMatches(matches[0],asmIdPattern);
            //            if (ids.Count > 0)
            //            {
            //                string v = "", p = "";
            //                id = ids[0];
            //                try
            //                {
            //                    // Get major and minor version number
            //                    List<string> vs = JS.Library.Helper.Strings.GetRegexMatches(matches[0], majVersionPattern);
            //                    List<string> ps = JS.Library.Helper.Strings.GetRegexMatches(matches[0], minVersionPattern);
            //                    v = vs.Count>0 ? vs[0] : "V0";
            //                    p = ps.Count>0 ? ps[0] : "p0";
            //                    Int32.TryParse(v.Replace("V",""), out majorrelease);
            //                    Int32.TryParse(p.Replace("p",""), out minorrelease);
            //                }
            //                catch (Exception ex)
            //                {
            //                    MessageBox.Show("Error getting maj/min version "+ matches[0].ToString() + "v="+v +" p="+p+" "+ex.Message);
            //                }
            //            }
                        
            //            n = c.Name.ToString().Remove(0, c.Name.ToString().IndexOf(" ") + 1);
                       
            //        }
            //        else if (JS.Library.Helper.Strings.GetRegexMatches(c.Name, prtPattern).Count > 0)
            //        {
            //            matches = JS.Library.Helper.Strings.GetRegexMatches(c.Name, prtPattern);

            //            // Get assembly or part id number
            //            List<string> ids = new List<string>();

            //            ids = JS.Library.Helper.Strings.GetRegexMatches(matches[0], prtIdPattern);

            //            if (ids.Count > 0)
            //            {
            //                string v = "", p = "";
            //                id = ids[0];
            //                try
            //                {
            //                    // Get major and minor version number
            //                    List<string> vs = JS.Library.Helper.Strings.GetRegexMatches(matches[0], majVersionPattern);
            //                    List<string> ps = JS.Library.Helper.Strings.GetRegexMatches(matches[0], minVersionPattern);
            //                    v = vs.Count > 0 ? vs[0] : "V0";
            //                    p = ps.Count > 0 ? ps[0] : "p0";
            //                    Int32.TryParse(v.Replace("V", ""), out majorrelease);
            //                    Int32.TryParse(p.Replace("p", ""), out minorrelease);
            //                }
            //                catch (Exception ex)
            //                {
            //                    MessageBox.Show("Error getting maj/min version " + matches[0].ToString() + "v=" + v + " p=" + p + " " + ex.Message);
            //                }
            //            }
            //            n = c.Name.ToString().Remove(0, c.Name.ToString().IndexOf(" ") + 1);

            //        }
            //        else if (JS.Library.Helper.Strings.GetRegexMatches(c.Name, stdPattern).Count > 0)
            //        {
            //            matches = JS.Library.Helper.Strings.GetRegexMatches(c.Name, stdPattern);
                       
            //            // Get assembly or part id number
            //            List<string> ids = new List<string>();

            //            ids = JS.Library.Helper.Strings.GetRegexMatches(matches[0], stdPattern);

            //            if (ids.Count > 0)
            //            {
            //                string v = "", p = "";
            //                id = ids[0];
            //                try
            //                {
            //                    // Get major and minor version number
            //                    List<string> vs = JS.Library.Helper.Strings.GetRegexMatches(matches[0], majVersionPattern);
            //                    List<string> ps = JS.Library.Helper.Strings.GetRegexMatches(matches[0], minVersionPattern);
            //                    v = vs.Count > 0 ? vs[0] : "V0";
            //                    p = ps.Count > 0 ? ps[0] : "p0";
            //                    Int32.TryParse(v.Replace("V", ""), out majorrelease);
            //                    Int32.TryParse(p.Replace("p", ""), out minorrelease);
            //                }
            //                catch (Exception ex)
            //                {
            //                    MessageBox.Show("Error getting maj/min version " + matches[0].ToString() + "v=" + v + " p=" + p + " " + ex.Message);
            //                }
            //            }
            //            n = c.Name.ToString().Remove(0, c.Name.ToString().IndexOf(" ") + 1);
            //        }
            //        else
            //        {
            //            id = "-";
            //            majorrelease = 0;
            //            minorrelease = 0;
            //            n = c.Name.ToString();
            //        }
            //        n = n.Remove(n.LastIndexOf("-"));

            //        JS.Library.SolidWorks.SWAssembly.AsmComponent comp = new JS.Library.SolidWorks.SWAssembly.AsmComponent(id, n, majorrelease, minorrelease, 1);

            //        if (entries.Find(x=>(x.id==comp.id) &&( x.majorversion == comp.majorversion) && (x.minorversion == comp.minorversion) && (x.name == comp.name))!=null)
            //        {
            //            entries.Find(x => (x.id == comp.id) && (x.majorversion == comp.majorversion) && (x.minorversion == comp.minorversion) && (x.name == comp.name)).quantity++;
            //        }
            //        else
            //        {
            //            entries.Add(comp);
            //        }
                   
            //        ++i;
            //    }

                
            //}

            
            //StringBuilder sb = new StringBuilder();
            //foreach(JS.Library.SolidWorks.SWAssembly.AsmComponent c in entries)
            //{
            //    sb.Append(c.ToString() + "\n");
            //}
            //string msgtext = sb.ToString();

            //if (MessageBox.Show(sb.ToString(), "ASM content, press OK to copy to clipboard", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.OK)
            //{
            //    Clipboard.SetText(msgtext);
            //}


            #endregion


        }

        private void RBtnCurrentViewstate_CheckedChanged(object sender, EventArgs e)
        {
            if (this.RBtnCurrentViewstate.Checked)
            {
                this.selection = JS.Library.SolidWorks.SWAssembly.SWAssemblyHelper.ComponentSelection.VISIBLE;
            }
        }
        
        private void RbtnUnsuppressed_CheckedChanged(object sender, EventArgs e)
        {
            this.selection = JS.Library.SolidWorks.SWAssembly.SWAssemblyHelper.ComponentSelection.UNSUPPRESED;
        }

        private void RBtnSuppressed_CheckedChanged(object sender, EventArgs e)
        {
            this.selection = JS.Library.SolidWorks.SWAssembly.SWAssemblyHelper.ComponentSelection.ALL;
        }

        private void RBtnTopLevel_CheckedChanged(object sender, EventArgs e)
        {
            this.asmExportOptions = AsmExportOptions.TopLevel;
        }

        private void RBtnRecursivePartsOnly_CheckedChanged(object sender, EventArgs e)
        {
            this.asmExportOptions = AsmExportOptions.RecursiveParts;
        }

        private void RBtnRecursive2_CheckedChanged(object sender, EventArgs e)
        {
            this.asmExportOptions = AsmExportOptions.RecursiveIndent;
        }

        private void TTip1_Popup(object sender, PopupEventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void BtnPushData_Click(object sender, EventArgs e)
        {
            // find the object in the object's list that's GetName-Method returns the same as in the dropdown menu
            // or just use the index, dropdown-item index and object list index should match



            if (this.objFromLoadedAssemblies != null && this.objFromLoadedAssemblies.Count > 0)
            {
                int idx = this.CbxExportMethod.SelectedIndex;
                if (this.objFromLoadedAssemblies.Count - 1 >= idx)
                {
                    object o = objFromLoadedAssemblies[idx];
                    MethodInfo mi = o.GetType().GetMethod("PushData");
                    List<string> testentries = new List<string>();
                    testentries.Add("first");
                    testentries.Add("second");
                    string format = "formatstring";
                    object[] parameters = new object[2];
                    parameters[0] = testentries;
                    parameters[1] = format;

                    bool result = (bool)mi.Invoke(o, parameters);
                }
                else
                {
                    MessageBox.Show("Index not foound");
                }

            }
            else
            {
                if (this.objFromLoadedAssemblies == null)
                {
                    MessageBox.Show("Objectlist null");
                }
                else if (this.objFromLoadedAssemblies.Count <= 0)
                {
                    MessageBox.Show("Objectlist count 0");
                }

            }
        }
    }


    public class ComboboxItem
    {
        public string Text { get; set; }
        public object Value { get; set; }

        public override string ToString()
        {
            return Text;
        }
    }

}
