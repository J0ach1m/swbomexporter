﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWBoMExport
{
   
    public partial class SWInstanceSelector : Form
    {

        public int choosenInstance;

        public SWInstanceSelector(List<string> objlist)
        {
            InitializeComponent();
            foreach(string itemtext in objlist)
            {
                ComboboxItem item = new ComboboxItem();
                item.Text = itemtext;
                this.CBx1.Items.Add(item);

            }
            this.Lbl1.Text= "Multiple SolidWorks instances found.\nClose unneeded or choose desired instance below.";
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            this.choosenInstance = this.CBx1.SelectedIndex;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }

    public class ComboboxItem
    {
        public string Text { get; set; }
        public override string ToString()
        {
            return Text;
        }
    }
}
