﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SWBlankAddin
{
    public partial class OutputWindow : Form
    {
        public OutputWindow()
        {
            InitializeComponent();
            this.ListView1.Columns.Add("Id");
            this.ListView1.Columns.Add("Version");
            this.ListView1.Columns.Add("Name");
            this.ListView1.Columns.Add("Subtype");
            this.ListView1.Columns.Add("Quantity");
            this.ListView1.Columns.Add("Comment");
            this.ListView1.View = View.Details;
        }

        public void PopulateDataTable(List<JS.Library.SolidWorks.SWAssembly.AsmComponent> lines)
        {
            
            if (lines.Count <= 0)
            {
                return;
            }
            
            foreach (JS.Library.SolidWorks.SWAssembly.AsmComponent ac in lines)
            {
                string v = string.Format("{0}.{1}", ac.majorversion.ToString(), ac.minorversion.ToString());

                string[] row = { ac.id, v, ac.name.ToString(), ac.subtype.ToString(), ac.quantity.ToString(),ac.rawdata };
                var listviewitem = new ListViewItem(row);
                this.ListView1.Items.Add(listviewitem);
            }


            StringBuilder sb = new StringBuilder();
            sb.Append(lines.Count().ToString() + " components of "
                +lines.Select(x=>x.componenttypename).Distinct().Count().ToString()
                +" different types found. (");
            foreach(string c in lines.Select(x => x.componenttypename).Distinct())
            {
                sb.Append(lines.Count(x => x.componenttypename.Equals(c))+" "+c+", ");
            }
            sb.Length = sb.Length - 2;
            sb.Append(")");
            string resulttext = sb.ToString();
            this.LblResults.Text = resulttext;
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
